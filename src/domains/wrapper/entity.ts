export interface HTTPListResponse<T = Record<string, never>> {
  message: string;
  length: number;
  data: T;
}

export interface ActionOptions {
  pageLoading?: boolean;
}

export interface ListMeta {
  length?: number;
}

export interface HTTPResponse<T = Record<string, never>> {
  status: number | string;
  message: string;
  data: T;
}
