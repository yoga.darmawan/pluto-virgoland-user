export interface UserListQuery {
  count?: string;
  page_number?: string;
  email?: string;
  status?: string;
  role?: string;
}

export interface Permission {
  name: string;
  description?: string;
}

export interface Role {
  id: number;
  name: string;
  description?: string;
  permissions?: Permission[];
}

export enum UserStatusEnum {
  INVITED = 6,
  ACTIVE = 1,
  BLOCKED = 3,
  DEACTIVATED = 2,
}

export interface User {
  email: string;
  id: string;
  last_login: string;
  status: number;
  roles: Role[];
  reason?: string;
}

export enum CreateUserEnum {
  REQUESTOR_ID = 2,
}

export interface CreateUserPayload {
  email: string;
  requestor_id: CreateUserEnum.REQUESTOR_ID;
  role_id: number[];
}

export interface EditUserPayload {
  requestor_id: CreateUserEnum.REQUESTOR_ID;
  user_id: string;
  roles_id: number[];
}

export interface EditUserStatusPayload {
  status: number;
  user_id: string;
  reason?: string;
}

export interface ReinviteUserPayload {
  email: string;
}

export interface DeactivateUserPayload {
  user_name: string;
}
