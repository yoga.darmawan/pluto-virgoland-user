import { HTTPListResponse, HTTPResponse } from '../wrapper/entity';
import {
  UserListQuery,
  User,
  Role,
  CreateUserPayload,
  EditUserPayload,
  EditUserStatusPayload,
  ReinviteUserPayload,
  DeactivateUserPayload,
} from './entity';

export default interface ManagementRepository {
  requestUserList: (query?: UserListQuery) => Promise<HTTPListResponse<User[]>>;
  requestUserById: (id: string) => Promise<HTTPResponse<User>>;
  requestRoleList: () => Promise<HTTPListResponse<Role[]>>;
  requestCreateUser: (payload: CreateUserPayload) => Promise<HTTPResponse>;
  requestEditUser: (payload: EditUserPayload) => Promise<HTTPResponse>;
  requestEditStatusUser: (payload: EditUserStatusPayload) => Promise<HTTPResponse>;
  requestReinviteUser: (payload: ReinviteUserPayload) => Promise<HTTPResponse>;
  requestDeactivateUser: (payload: DeactivateUserPayload) => Promise<HTTPResponse>;
}
