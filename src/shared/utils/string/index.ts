/* eslint-disable import/prefer-default-export */
export const stringPermission = {
  // Role
  rolePermissionList: 'settings.manage-role.list',
  rolePermissionCreate: 'settings.manage-role.create',
  rolePermissionEdit: 'settings.manage-role.edit',
  rolePermissionDelete: 'settings.manage-role.delete',

  // User
  userList: 'settings.manage-users.list',
  userCreate: 'settings.manage-users.create',
  userEdit: 'settings.manage-users.edit',
  userBlock: 'settings.manage-users.block',
  userUnblock: 'settings.manage-users.unblock',
  userDeactivate: 'settings.manage-users.deactivate',
  userReinvite: 'settings.manage-users.reinvite',
};
