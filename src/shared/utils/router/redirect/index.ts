import React from 'react';
import { useNavigate } from 'react-router-dom';
import { stringifyUrl } from 'query-string';

type Name = 'UserManagementList' | 'UserManagementForm';

interface Params {
  url?: string;
  name?: Name;
  query?: {
    [key: string]: string | number;
  };
  id?: string | number;
  pushType?: 'push' | 'replace';
}

export const generatePath = ({ name, id, query }: Params): string => {
  let url = '';
  switch (name) {
    case 'UserManagementList':
      url = `${stringifyUrl({
        url: '/management/user',
        query,
      })}`;
      break;
    case 'UserManagementForm':
      url = '/management/user/form';
      url += id ? `/${id}` : '';
      break;
    default:
      break;
  }
  return url;
};

export const useRedirect = () => {
  const navigate = useNavigate();

  const redirect = React.useCallback(
    ({ name, id, query, url, pushType = 'push' }: Params): void => {
      let path = '';
      if (url) {
        path = url;
      } else {
        path = generatePath({ name, id, query });
      }
      navigate(path, {
        replace: pushType === 'replace',
      });
    },
    [],
  );

  const goBack = React.useCallback(() => {
    navigate(-1);
  }, []);

  const locationRedirect = React.useCallback((url: string) => {
    window.location.href = stringifyUrl({ url });
  }, []);

  return {
    redirect,
    locationRedirect,
    goBack,
  };
};
