import React from 'react';
import { useLocation } from 'react-router-dom';
import { parse, stringify } from 'query-string';

export const useQueryParamsState = <S>() => {
  const mounted = React.useRef(false);
  const IS = React.useRef({});
  const [state, setState] = React.useState<S>(null);
  const { search } = useLocation();

  const appendState = React.useCallback((prevState, inState) => {
    const keys = Object.keys(prevState);
    let newState = { ...prevState };
    if (Object.keys(prevState).length > 0 && Object.keys(inState).length === 0) {
      newState = IS.current;
    } else {
      keys.forEach((key: string) => {
        if (inState[key] !== undefined) {
          newState[key] = inState[key];
        }
      });
    }
    setState(newState);
  }, []);

  React.useEffect(() => {
    if (mounted.current) {
      const parsed = parse(search);
      appendState(state, parsed);
    }
  }, [search]);

  const init = React.useCallback((inititalState: S) => {
    mounted.current = true;
    IS.current = inititalState;
    const parsed = parse(search);
    appendState(inititalState, parsed);
  }, []);

  return {
    init,
    state,
    stringifyState: stringify(state || {}),
  };
};
