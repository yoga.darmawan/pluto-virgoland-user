import { useDispatch } from 'react-redux';

import { AppDispatch } from './store';
import { Loading, Config } from './types';
import { uiActions } from './ui-slice';

export default function Helper() {
  const dispatch: AppDispatch = useDispatch();

  function triggerLoading(payload: Loading) {
    return dispatch(uiActions.triggerLoading(payload));
  }

  function setConfig(payload: Config) {
    return dispatch(uiActions.setConfig(payload));
  }

  function triggerSidebar(payload: { open: boolean }) {
    return dispatch(uiActions.triggerSidebar(payload));
  }

  return {
    triggerLoading,
    setConfig,
    triggerSidebar,
  };
}
