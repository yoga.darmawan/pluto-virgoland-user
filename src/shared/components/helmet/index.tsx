import { Helmet as RH } from 'react-helmet';

interface Props {
  title: string;
}

const getTitleTemplate = (title: string) => `${title} | Virgoland`;

const Helmet = ({ title }: Props) => {
  return (
    <RH>
      <title>{getTitleTemplate(title)}</title>
    </RH>
  );
};

export default Helmet;
