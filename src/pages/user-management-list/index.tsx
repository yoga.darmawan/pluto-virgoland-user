import React from 'react';

import UserManagementList from '@/modules/management/presentation/interface/user-management-list';

const UserManagementListPage = () => {
  return (
    <>
      <UserManagementList />
    </>
  );
};

export default UserManagementListPage;
