import { loadable } from '@/shared/utils/loadable';

const UserManagementListPage = loadable(() => import('./index'));

const config = {
  path: '/management/user',
  key: 'user-management-list-page',
  component: <UserManagementListPage />,
};

export default config;
