import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { render, cleanup, screen } from '@testing-library/react';

import { ThemeProvider } from '@/theme/index';
import { store } from '@/core/redux/store';

import UserManagementListPage from '../index';

const Component = (
  <ReduxProvider store={store}>
    <UserManagementListPage />
  </ReduxProvider>
);

jest.mock(
  '@virgoland/layout',
  () => {
    return {
      hasPermission: () => true,
    };
  },
  { virtual: true },
);

describe('User Management List Page Tests Group', () => {
  afterEach(() => {
    cleanup();
  });
  // 1. Check Component Rendered
  it('1. Check Component Rendered', async () => {
    // unit test begin
    const wrapper = render(Component, { wrapper: ThemeProvider });
    expect(wrapper).toBeTruthy();
  });
});
