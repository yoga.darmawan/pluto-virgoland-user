import { Routes, Route } from 'react-router-dom';

import UserManagementListPage from './user-management-list/loadable';
import UserManagementFormPage from './user-management-form/loadable';

const generateRoutes = (pages) => {
  return (
    <Routes>
      {pages.map(({ key, path, component }) => (
        <Route key={key} path={path} element={component} />
      ))}
    </Routes>
  );
};

export const Privates = () => {
  return <>{generateRoutes([UserManagementListPage, ...UserManagementFormPage])}</>;
};
