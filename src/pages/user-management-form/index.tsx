import React from 'react';

import UserManagementForm from '@/modules/management/presentation/interface/user-management-form';

const UserManagementFormPage = () => {
  return (
    <>
      <UserManagementForm />
    </>
  );
};

export default UserManagementFormPage;
