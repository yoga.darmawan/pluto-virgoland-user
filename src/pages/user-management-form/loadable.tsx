import { loadable } from '@/shared/utils/loadable';

const UserManagementFormPage = loadable(() => import('./index'));

const config = [
  {
    path: '/management/user/form',
    key: 'user-management-list-page',
    component: <UserManagementFormPage />,
  },
  {
    path: '/management/user/form/:id',
    key: 'user-management-list-page',
    component: <UserManagementFormPage />,
  },
];

export default config;
