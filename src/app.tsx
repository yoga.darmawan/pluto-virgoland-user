import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { store } from '@/core/redux/store';
import { ThemeProvider } from '@/theme/index';
import PageWrapper from '@/shared/components/page-wrapper';

import Pages from './pages';

export default function App() {
  return (
    <ReduxProvider store={store}>
      <ThemeProvider>
        <PageWrapper>
          <BrowserRouter>
            <Pages />
          </BrowserRouter>
        </PageWrapper>
      </ThemeProvider>
    </ReduxProvider>
  );
}
