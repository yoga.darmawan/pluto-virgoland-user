import { Role, User } from '@/domains/management/entity';
import { ListMeta } from '@/domains/wrapper/entity';

export interface InitialState {
  users: User[];
  user: User;
  usersMeta: ListMeta;
  roles: Role[];
}
