import { useDispatch } from 'react-redux';

import { AppThunk } from '@/shared/core/redux/store';
import Helper from '@/shared/core/redux/helper';
import { usecase } from '@/modules/management/util';
import {
  CreateUserPayload,
  DeactivateUserPayload,
  EditUserPayload,
  EditUserStatusPayload,
  ReinviteUserPayload,
  UserListQuery,
} from '@/domains/management/entity';
import { ActionOptions } from '@/domains/wrapper/entity';

import { managementActions } from './slice';

export default function ActionModel() {
  const dispatch = useDispatch();
  const { triggerLoading, ...helperAction } = Helper();

  function requestUserList(
    query?: UserListQuery,
    successCb?: (response: any) => void,
    { pageLoading }: ActionOptions = {},
  ): AppThunk {
    return async () => {
      try {
        if (pageLoading) {
          // trigger loading
        }
        const response = await usecase.requestUserList(query);
        dispatch(managementActions.setUsers(response.data));
        dispatch(managementActions.setUsersMeta({ length: response.length }));
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        if (pageLoading) {
          // trigger loading
        }
      }
    };
  }

  function requestUserById(
    id: string,
    successCb?: (response: any) => void,
    { pageLoading }: ActionOptions = {},
  ): AppThunk {
    return async () => {
      try {
        if (pageLoading) {
          // trigger loading
        }
        const response = await usecase.requestUserById(id);
        dispatch(managementActions.setUser(response.data));
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        if (pageLoading) {
          // trigger loading
        }
      }
    };
  }

  function requestRoleList(
    successCb?: (response: any) => void,
    { pageLoading }: ActionOptions = {},
  ): AppThunk {
    return async () => {
      try {
        if (pageLoading) {
          // trigger loading
        }
        const response = await usecase.requestRoleList();
        dispatch(managementActions.setRoles(response.data));
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        if (pageLoading) {
          // trigger loading
        }
      }
    };
  }

  function requestCreateUser(
    payload: CreateUserPayload,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      try {
        triggerLoading({
          primaryAction: true,
        });
        const response = await usecase.requestCreateUser(payload);
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        triggerLoading({
          primaryAction: false,
        });
      }
    };
  }

  function requestEditUser(
    payload: EditUserPayload,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      try {
        triggerLoading({
          primaryAction: true,
        });
        const response = await usecase.requestEditUser(payload);
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        triggerLoading({
          primaryAction: false,
        });
      }
    };
  }

  function requestEditStatusUser(
    payload: EditUserStatusPayload,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      try {
        triggerLoading({
          primaryAction: true,
        });
        const response = await usecase.requestEditStatusUser(payload);
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        triggerLoading({
          primaryAction: false,
        });
      }
    };
  }

  function requestReinviteUser(
    payload: ReinviteUserPayload,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      try {
        triggerLoading({
          primaryAction: true,
        });
        const response = await usecase.requestReinviteUser(payload);
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        triggerLoading({
          primaryAction: false,
        });
      }
    };
  }

  function requestDeactivateUser(
    payload: DeactivateUserPayload,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      try {
        triggerLoading({
          primaryAction: true,
        });
        const response = await usecase.requestDeactivateUser(payload);
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        triggerLoading({
          primaryAction: false,
        });
      }
    };
  }

  return {
    requestUserList: (
      query?: UserListQuery,
      successCb?: (response: any) => void,
      options?: ActionOptions,
    ) => dispatch(requestUserList(query, successCb, options)),

    requestUserById: (id?: string, successCb?: (response: any) => void, options?: ActionOptions) =>
      dispatch(requestUserById(id, successCb, options)),

    requestRoleList: (successCb?: (response: any) => void, options?: ActionOptions) =>
      dispatch(requestRoleList(successCb, options)),

    requestCreateUser: (payload: CreateUserPayload, successCb?: (response: any) => void) =>
      dispatch(requestCreateUser(payload, successCb)),

    requestEditUser: (payload: EditUserPayload, successCb?: (response: any) => void) =>
      dispatch(requestEditUser(payload, successCb)),

    requestEditStatusUser: (payload: EditUserStatusPayload, successCb?: (response: any) => void) =>
      dispatch(requestEditStatusUser(payload, successCb)),

    requestReinviteUser: (payload: ReinviteUserPayload, successCb?: (response: any) => void) =>
      dispatch(requestReinviteUser(payload, successCb)),

    requestDeactivateUser: (payload: DeactivateUserPayload, successCb?: (response: any) => void) =>
      dispatch(requestDeactivateUser(payload, successCb)),

    ...helperAction,
  };
}
