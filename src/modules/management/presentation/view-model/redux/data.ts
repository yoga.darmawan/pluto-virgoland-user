import { useSelector } from 'react-redux';

import { RootState } from '@/shared/core/redux/store';
import DataModel from '@/shared/core/redux/data';
import { Role, User } from '@/domains/management/entity';

export default function ManagementDataModel() {
  const dataModel = DataModel();
  const users: User[] = useSelector((state: RootState) => state.management.users);
  const user: User = useSelector((state: RootState) => state.management.user);
  const usersMeta = useSelector((state: RootState) => state.management.usersMeta);
  const roles: Role[] = useSelector((state: RootState) => state.management.roles);

  return {
    users,
    user,
    usersMeta,
    roles,
    ...dataModel,
  };
}
