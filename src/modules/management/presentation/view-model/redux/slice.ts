import { createSlice } from '@reduxjs/toolkit';

import { InitialState } from './types';

const initAuth: InitialState = {
  users: [],
  user: {
    id: '',
    email: '',
    last_login: '',
    status: null,
    roles: [],
  },
  usersMeta: {
    length: 0,
  },
  roles: [],
};

// [NOTE]: bugs https://github.com/microsoft/TypeScript/issues/42873
// temporary fix: adding pnpm override immer and install immer in package.json
// also specify path to immer in tsconfig.json
const managementSlice: any = createSlice({
  name: 'management',
  initialState: {
    ...initAuth,
  },
  reducers: {
    setUsers(state, { payload }) {
      state.users = payload || [];
    },
    setUser(state, { payload }) {
      state.user = payload || [];
    },
    setUsersMeta(state, { payload }) {
      state.usersMeta = payload || {};
    },
    setRoles(state, { payload }) {
      state.roles = payload || [];
    },
  },
});

export const managementActions = managementSlice.actions;

export default managementSlice;
