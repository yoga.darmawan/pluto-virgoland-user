import { injectReducer } from '@/core/redux/store';
import managementSlice from './redux/slice';
import dataModel from './redux/data';
import actionModel from './redux/action';

export default function ManagementViewModel() {
  injectReducer('management', managementSlice.reducer);
  return {
    dataModel: () => dataModel(),
    actionModel: () => actionModel(),
  };
}
