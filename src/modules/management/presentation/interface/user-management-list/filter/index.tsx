import React from 'react';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Button from '@mui/material/Button';

import { UserListQuery, UserStatusEnum } from '@/domains/management/entity';
import ManagementDataModel from '@/modules/management/presentation/view-model/redux/data';
import { useRedirect } from '@/shared/utils/router';

import { Wrapper, paperSx } from './style';

interface Option {
  id: number;
  name: string;
}

const StatusOptions: Option[] = Object.keys(UserStatusEnum)
  .filter((k) => isNaN(Number(k)))
  .map((status) => {
    return {
      id: UserStatusEnum[status],
      name: status,
    };
  });

const UserManagementListFilter = (props: { queryState: UserListQuery }) => {
  // INIT
  const { roles } = ManagementDataModel();
  // HOOKS
  const [state, setState] = React.useState<UserListQuery>({});
  // CUSTOM HOOKS
  const { redirect } = useRedirect();

  React.useEffect(() => {
    setState(props.queryState);
  }, [props.queryState]);

  const onChange = React.useCallback((key: keyof UserListQuery, value: string | Option[]) => {
    let newValue = '';
    if (key === 'email' && typeof value === 'string') {
      newValue = value;
    } else if (value && typeof value !== 'string' && value.length > 0) {
      newValue = value.reduce(
        (prevStr, inStr) => `${prevStr}${prevStr !== '' ? ',' : ''}${inStr.id}`,
        '',
      );
    }
    setState((prevState) => ({
      ...prevState,
      [key]: newValue,
    }));
  }, []);

  const onApplyFilter = () => {
    redirect({
      name: 'UserManagementList',
      query: {
        ...props.queryState,
        ...state,
      },
    });
  };

  return (
    <Wrapper>
      <Paper elevation={6} sx={paperSx}>
        <Box>
          <h2 className="filter_title">Filter</h2>
        </Box>
        <Box sx={{ flexGrow: 1 }}>
          <Grid container spacing={2} alignItems="start">
            <Grid item xs={3}>
              <div className="filter_label">User Email</div>
              <TextField
                id="email-input"
                placeholder="someone@somewhere.com"
                variant="outlined"
                size="small"
                fullWidth
                onChange={(e) => {
                  onChange('email', e.target.value);
                }}
                value={state?.email || ''}
              />
            </Grid>
            <Grid item xs={4}>
              <div className="filter_label">User Roles</div>
              <Autocomplete
                multiple
                id="select-multiple-role"
                options={roles || []}
                size="small"
                getOptionLabel={(option: any) => option.name}
                renderInput={(params) => (
                  <TextField {...params} variant="outlined" placeholder="Role(s)" fullWidth />
                )}
                onChange={(e, val) => onChange('role', val)}
              />
            </Grid>
            <Grid item xs={4}>
              <div className="filter_label">User Status</div>
              <Autocomplete
                multiple
                id="select-multiple-status"
                options={StatusOptions}
                size="small"
                getOptionLabel={(option: any) => option.name}
                renderInput={(params) => (
                  <TextField {...params} variant="outlined" placeholder="Status(es)" fullWidth />
                )}
                onChange={(e, val) => onChange('status', val)}
              />
            </Grid>
            <Grid item xs={2}>
              <Button type="button" variant="contained" onClick={onApplyFilter}>
                Search
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    </Wrapper>
  );
};

export default UserManagementListFilter;
