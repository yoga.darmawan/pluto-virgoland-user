import styled from '@emotion/styled';
import { SxProps } from '@mui/material';

export const Wrapper = styled.div`
  .modal_title {
    font-size: 1.2rem;
    padding: 1.5rem 1.5rem 1rem 1.5rem;
    border-bottom: ${({
      theme: {
        palette: { mist },
      },
    }) => `1px solid ${mist.main}`};
  }
  .modal_cta {
    padding: 1.5rem;
    border-bottom: ${({
      theme: {
        palette: { mist },
      },
    }) => `1px solid ${mist.main}`};
    textarea {
      width: 100%;
      padding: 0.5rem;
      box-sizing: border-box;
      outline: none;
    }
  }
  .modal_action {
    padding: 1rem 1.5rem;
    display: flex;
    justify-content: end;
    button {
      &:first-of-type {
        margin-right: 1rem !important;
      }
    }
  }
`;

export const contentSx: SxProps = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: 500,
  bgcolor: 'background.paper',
  boxShadow: 24,
  outline: 'none',
  borderRadius: 1,
};
