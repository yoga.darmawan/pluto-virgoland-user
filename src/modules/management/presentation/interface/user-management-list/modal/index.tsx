import React, { ChangeEvent } from 'react';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';

import DataModel from '@/shared/core/redux/data';
import ActionModel from '@/modules/management/presentation/view-model/redux/action';
import { User, UserStatusEnum } from '@/domains/management/entity';

import type { ModalType } from '../table';
import { Wrapper, contentSx } from './style';

interface Props {
  open: boolean;
  onClose: (reload?: boolean) => void;
  type: ModalType;
  user?: User;
}

const UserManagementListModal = ({ open, type, onClose, user }: Props) => {
  // HOOKS
  const [reason, setReason] = React.useState('');
  // CUSTOM HOOKS
  const { requestEditStatusUser, requestReinviteUser, requestDeactivateUser } = ActionModel();
  const {
    loading: { primaryAction },
  } = DataModel();

  const copy = React.useMemo(() => {
    if (type === 'reinvite') {
      return 'Confirm Reinvite User ?';
    } else if (type === 'deactivate') {
      return 'Confirm Deactivate ?';
    } else if (type === 'block') {
      return 'Confirm Block ?';
    } else if (type === 'unblock') {
      return 'Confirm Unblock ?';
    }
  }, [type]);

  const onSubmit = React.useCallback(() => {
    if (type === 'reinvite') {
      requestReinviteUser(
        {
          email: user.email,
        },
        () => onClose(true),
      );
    } else if (type === 'deactivate') {
      requestDeactivateUser(
        {
          user_name: user.email,
        },
        () => onClose(true),
      );
    } else if (type === 'block') {
      requestEditStatusUser(
        {
          status: UserStatusEnum.BLOCKED,
          user_id: user.id,
          reason,
        },
        () => onClose(true),
      );
    } else if (type === 'unblock') {
      requestEditStatusUser(
        {
          status: UserStatusEnum.ACTIVE,
          user_id: user.id,
        },
        () => onClose(true),
      );
    }
  }, [reason, type, user]);

  const onChangeReason = React.useCallback((e: ChangeEvent<HTMLTextAreaElement>) => {
    setReason(e.target.value);
  }, []);

  const body = React.useMemo(() => {
    if (type === 'block') {
      return (
        <>
          <div className="modal_title">{copy}</div>
          <div className="modal_cta">
            <textarea
              name="reason"
              rows={5}
              placeholder="Enter the reason for block"
              onChange={(e) => onChangeReason(e)}
            >
              {reason}
            </textarea>
          </div>
        </>
      );
    } else if (type === 'unblock') {
      return (
        <>
          <div className="modal_title">{copy}</div>
          <div className="modal_cta">
            <p>{user.reason}</p>
          </div>
        </>
      );
    }
    return (
      <div className="modal_cta">
        <p>{copy}</p>
      </div>
    );
  }, [copy, type, user.reason]);

  return (
    <Modal open={open} onClose={onClose}>
      <Box sx={contentSx}>
        <Wrapper>
          {body}
          <div className="modal_action">
            <Button variant="outlined" onClick={() => onClose()}>
              Batal
            </Button>
            <LoadingButton
              variant="contained"
              disabled={type === 'block' && reason === ''}
              loading={primaryAction}
              onClick={onSubmit}
            >
              OK
            </LoadingButton>
          </div>
        </Wrapper>
      </Box>
    </Modal>
  );
};

export default UserManagementListModal;
