import React from 'react';

import ManagementViewModel from '../../view-model';
import { useQueryParamsState } from '@/shared/utils/router';
import { UserListQuery } from '@/domains/management/entity';
import Helmet from '@/shared/components/helmet';

import Filter from './filter';
import Table from './table';

const UserManagementList = () => {
  // INIT STORE
  const { actionModel } = (() => ManagementViewModel())();
  const { requestUserList, requestRoleList } = actionModel();
  // HOOKS
  const [reload, setReload] = React.useState(false);
  // CUSTOM HOOKS
  const { init, state, stringifyState } = useQueryParamsState<UserListQuery>();

  React.useEffect(() => {
    requestRoleList();
    init({
      count: '10',
      page_number: '1',
      email: '',
      role: '',
      status: '',
    });
  }, []);

  React.useEffect(() => {
    if (stringifyState) {
      requestUserList(state || {});
    }
  }, [stringifyState, reload]);

  return (
    <>
      <Helmet title="Management User List" />
      <Filter queryState={state || {}} />
      <Table queryState={state || {}} setReload={setReload} />
    </>
  );
};

export default UserManagementList;
