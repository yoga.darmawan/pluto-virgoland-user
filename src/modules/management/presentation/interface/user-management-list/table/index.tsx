import React from 'react';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { Link } from 'react-router-dom';

import ManagementDataModel from '@/modules/management/presentation/view-model/redux/data';
import { User, UserListQuery, UserStatusEnum } from '@/domains/management/entity';
import { useRedirect, generatePath } from '@/shared/utils/router';

import Modal from '../modal';
import { Wrapper, paperSx, topBoxSx, actionBtnSx } from './style';

export type ModalType = 'reinvite' | 'deactivate' | 'block' | 'unblock';

interface Props {
  queryState: UserListQuery;
  setReload: (prev?: any) => any;
}

const UserManagementListTable = ({ queryState, setReload }: Props) => {
  // HOOKS
  const [openModal, setOpen] = React.useState(false);
  const [modalType, setModalType] = React.useState<ModalType>(null);
  const [selectedUser, setUser] = React.useState<User>({
    reason: '',
    id: '',
    email: '',
    last_login: '',
    status: null,
    roles: [],
  });
  // CUSTOM HOOKS
  const { users, usersMeta } = ManagementDataModel();
  const { redirect } = useRedirect();

  // CALLBACK
  const onOpenModal = React.useCallback((type: ModalType, user: User) => {
    setUser(user);
    setModalType(type);
    setOpen(true);
  }, []);

  const onCloseModal = React.useCallback((reload?: boolean) => {
    if (reload) {
      setReload((prev) => !prev);
    }
    setOpen(false);
  }, []);

  const getStatus = React.useCallback((status) => {
    let render = '';
    switch (status) {
      case UserStatusEnum.INVITED:
        render = 'Invited';
        break;
      case UserStatusEnum.ACTIVE:
        render = 'Active';
        break;
      case UserStatusEnum.BLOCKED:
        render = 'Blocked';
        break;
      case UserStatusEnum.DEACTIVATED:
        render = 'Deactivated';
        break;
      default:
        break;
    }
    return render;
  }, []);

  const onApplyFilter = (query: UserListQuery) => {
    redirect({
      name: 'UserManagementList',
      query: {
        ...queryState,
        ...query,
      },
    });
  };

  const getActionBtn = React.useCallback((user: User) => {
    const deacBtn = (
      <Button
        type="button"
        variant="outlined"
        sx={actionBtnSx}
        onClick={() => onOpenModal('deactivate', user)}
      >
        Deactivate
      </Button>
    );
    if (user.status === UserStatusEnum.ACTIVE) {
      return (
        <>
          <Button
            type="button"
            variant="outlined"
            sx={actionBtnSx}
            onClick={() => onOpenModal('block', user)}
          >
            Block
          </Button>
          {deacBtn}
        </>
      );
    } else if (user.status === UserStatusEnum.BLOCKED) {
      return (
        <>
          <Button
            type="button"
            variant="outlined"
            sx={actionBtnSx}
            onClick={() => onOpenModal('unblock', user)}
          >
            Unblock
          </Button>
          {deacBtn}
        </>
      );
    } else if (user.status === UserStatusEnum.INVITED) {
      return (
        <>
          <Button
            type="button"
            variant="outlined"
            sx={actionBtnSx}
            onClick={() => onOpenModal('reinvite', user)}
          >
            Reinvite
          </Button>
          {deacBtn}
        </>
      );
    }
  }, []);

  return (
    <Wrapper>
      <Modal open={openModal} type={modalType} onClose={onCloseModal} user={selectedUser} />
      <Paper elevation={6} sx={paperSx}>
        <Box sx={topBoxSx}>
          <h2 className="table_title">Data Table</h2>
          <Link
            to={generatePath({
              name: 'UserManagementForm',
            })}
          >
            <Button type="button" variant="contained">
              Add New User
            </Button>
          </Link>
        </Box>
        <Box>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>User Email</TableCell>
                  <TableCell align="left">Role</TableCell>
                  <TableCell align="center">Last Login</TableCell>
                  <TableCell align="center">Status</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {users &&
                  users.length > 0 &&
                  users.map((user: User) => (
                    <TableRow
                      key={user.id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {user.email}
                      </TableCell>
                      <TableCell align="left">
                        {user.roles.map((role) => (
                          <div key={role.id}>{role.name}</div>
                        ))}
                      </TableCell>
                      <TableCell align="center">{user.last_login || '-'}</TableCell>
                      <TableCell align="center">{getStatus(user.status)}</TableCell>
                      <TableCell align="center">
                        {user.status !== 2 && (
                          <Button
                            type="button"
                            variant="outlined"
                            sx={actionBtnSx}
                            onClick={() => redirect({ name: 'UserManagementForm', id: user.id })}
                          >
                            Edit
                          </Button>
                        )}
                        {getActionBtn(user)}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, 50]}
            component="div"
            count={usersMeta.length}
            rowsPerPage={Number(queryState?.count) || 10}
            page={Number(queryState?.page_number) - 1 || 0}
            onPageChange={(e, val) => {
              onApplyFilter({ page_number: (val + 1).toString() });
            }}
            onRowsPerPageChange={(e) => {
              onApplyFilter({ page_number: '1', count: e.target.value.toString() });
            }}
          />
        </Box>
      </Paper>
    </Wrapper>
  );
};

export default UserManagementListTable;
