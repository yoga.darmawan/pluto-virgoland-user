import { SxProps } from '@mui/material';
import styled from '@emotion/styled';

export const Wrapper = styled.div`
  margin-bottom: 2rem;
  .table_title {
    margin-top: 0px;
  }
  a {
    text-decoration: none;
  }
`;

export const paperSx: SxProps = {
  padding: '2rem',
};

export const topBoxSx: SxProps = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginBottom: '2rem',
};

export const actionBtnSx: SxProps = {
  marginRight: '1rem',
  marginBottom: '1rem',
};
