import React from 'react';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useParams } from 'react-router-dom';

import { CreateUserEnum, Role } from '@/domains/management/entity';
import { useRedirect } from '@/shared/utils/router';
import ManagementViewModel from '@/modules/management/presentation/view-model';
import Helmet from '@/shared/components/helmet';

import { Wrapper, paperSx } from './style';

const scheme = Yup.object().shape({
  email: Yup.string().email('Invalid email address').required('Email is required'),
});

const UserManagementForm = () => {
  // INIT STORE
  const { actionModel, dataModel } = (() => ManagementViewModel())();
  const { requestRoleList, requestUserById, requestCreateUser, requestEditUser } = actionModel();
  const {
    roles,
    user,
    loading: { primaryAction },
  } = dataModel();
  // HOOKS
  const [choosenRoles, setRoles] = React.useState<number[]>([]);
  // CUSTOM HOOKS
  const { goBack, redirect } = useRedirect();
  const { id: paramId } = useParams();

  const onSubmit = React.useCallback(
    (email) => {
      const onRedirect = () => {
        redirect({ name: 'UserManagementList', pushType: 'replace' });
      };
      if (!paramId) {
        requestCreateUser(
          {
            requestor_id: CreateUserEnum.REQUESTOR_ID,
            email,
            role_id: choosenRoles,
          },
          onRedirect,
        );
      } else {
        requestEditUser(
          {
            user_id: paramId,
            roles_id: choosenRoles,
            requestor_id: CreateUserEnum.REQUESTOR_ID,
          },
          onRedirect,
        );
      }
    },
    [choosenRoles, paramId],
  );

  const { handleSubmit, handleChange, handleBlur, values, errors, touched, setFieldValue } =
    useFormik({
      initialValues: {
        email: '',
      },
      validationSchema: scheme,
      onSubmit: ({ email }) => {
        onSubmit(email);
      },
    });

  const onChangeRole = React.useCallback((id: number) => {
    setRoles((prevRoles) => {
      const index = prevRoles.findIndex((role) => role === id);
      if (index === -1) {
        return [...prevRoles, id];
      }
      const copy = [...prevRoles];
      copy.splice(index, 1);
      return copy;
    });
  }, []);

  const isChoosen = React.useCallback(
    (id: number) => {
      return choosenRoles.findIndex((role) => role === id) !== -1;
    },
    [choosenRoles],
  );

  React.useEffect(() => {
    if (user) {
      setFieldValue('email', user.email);
      setRoles(user.roles.map((role) => role.id));
    }
  }, [user]);

  React.useEffect(() => {
    requestRoleList();
    if (paramId) {
      requestUserById(paramId);
    }
  }, []);

  return (
    <>
      <Helmet title="Management User Form" />
      <Wrapper>
        <Paper elevation={6} sx={paperSx}>
          <Box>
            <h2 className="form_title">{!paramId ? 'Create' : 'Edit'} User</h2>
          </Box>
          <Box>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={12} sm={6} md={6} lg={4}>
                <div className="form_label">User Email</div>
                <TextField
                  id="email-input"
                  placeholder="someone@somewhere.com"
                  variant="outlined"
                  size="small"
                  name="email"
                  fullWidth
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  error={errors.email && touched.email}
                  helperText={errors.email && touched.email ? errors.email : null}
                  disabled={paramId !== undefined}
                />
              </Grid>
              <Grid item xs={12}>
                <div className="form_label">Assign Role</div>
                <TableContainer component={Paper}>
                  <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell align="left">Role</TableCell>
                        <TableCell align="left">Permissions</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {roles.map((role: Role) => (
                        <TableRow
                          key={role.id}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell align="left" component="th" scope="row">
                            <Checkbox
                              color="primary"
                              value={role.id}
                              onChange={() => onChangeRole(role.id)}
                              checked={isChoosen(role.id)}
                            />
                            {role.name}
                          </TableCell>
                          <TableCell align="left">
                            {role.permissions.map((permission) => (
                              <div key={permission.name}>{permission.name}</div>
                            ))}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
              <Grid item xs={12} display="flex" justifyContent="end">
                <Button
                  type="button"
                  variant="outlined"
                  sx={{ marginRight: '1rem' }}
                  onClick={goBack}
                >
                  Back
                </Button>
                <LoadingButton
                  type="button"
                  variant="contained"
                  loading={primaryAction}
                  onClick={() => handleSubmit()}
                  disabled={choosenRoles.length === 0}
                >
                  Confirm Changes
                </LoadingButton>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </Wrapper>
    </>
  );
};

export default UserManagementForm;
