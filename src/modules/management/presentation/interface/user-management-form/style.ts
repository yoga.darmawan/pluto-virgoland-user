import { SxProps } from '@mui/material';
import styled from '@emotion/styled';

export const Wrapper = styled.div`
  margin-bottom: 2rem;
  .form_title {
    margin-top: 0px;
  }
  .form_label {
    font-weight: 900;
    margin-bottom: 10px;
  }
`;

export const paperSx: SxProps = {
  padding: '2rem',
};
