import {
  CreateUserPayload,
  DeactivateUserPayload,
  EditUserPayload,
  EditUserStatusPayload,
  ReinviteUserPayload,
  Role,
  User,
  UserListQuery,
} from '@/domains/management/entity';
import ManagementRepository from '@/domains/management/repository';
import ManagementUseCase from '@/domains/management/usecase';
import { HTTPListResponse, HTTPResponse } from '@/domains/wrapper/entity';

export default class ManagementUseCaseImpl implements ManagementUseCase {
  private repository: ManagementRepository;

  constructor(repository: ManagementRepository) {
    this.repository = repository;
  }

  async requestUserList(query?: UserListQuery): Promise<HTTPListResponse<User[]>> {
    try {
      const response: HTTPListResponse<User[]> = await this.repository.requestUserList(query);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestUserById(id: string): Promise<HTTPResponse<User>> {
    try {
      const response = await this.repository.requestUserById(id);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestRoleList(): Promise<HTTPListResponse<Role[]>> {
    try {
      const response: HTTPListResponse<Role[]> = await this.repository.requestRoleList();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestCreateUser(payload: CreateUserPayload): Promise<HTTPResponse> {
    try {
      const response: HTTPResponse = await this.repository.requestCreateUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestEditUser(payload: EditUserPayload): Promise<HTTPResponse> {
    try {
      const response: HTTPResponse = await this.repository.requestEditUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestEditStatusUser(payload: EditUserStatusPayload): Promise<HTTPResponse> {
    try {
      const response: HTTPResponse = await this.repository.requestEditStatusUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestReinviteUser(payload: ReinviteUserPayload): Promise<HTTPResponse> {
    try {
      const response: HTTPResponse = await this.repository.requestReinviteUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestDeactivateUser(payload: DeactivateUserPayload): Promise<HTTPResponse> {
    try {
      const response: HTTPResponse = await this.repository.requestDeactivateUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }
}
