import {
  CreateUserPayload,
  DeactivateUserPayload,
  EditUserPayload,
  EditUserStatusPayload,
  ReinviteUserPayload,
  Role,
  User,
  UserListQuery,
} from '@/domains/management/entity';
import ManagementRepository from '@/domains/management/repository';
import { HTTPListResponse, HTTPResponse } from '@/domains/wrapper/entity';
import ManagementFetcher from './fetcher';

export default class ManagementRepositoryImpl implements ManagementRepository {
  private fetcher: ManagementFetcher;

  constructor(fetcher: ManagementFetcher) {
    this.fetcher = fetcher;
  }

  async requestUserList(query?: UserListQuery): Promise<HTTPListResponse<User[]>> {
    try {
      const response = await this.fetcher.requestUserList(query);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestUserById(id: string): Promise<HTTPResponse<User>> {
    try {
      const response = await this.fetcher.requestUserById(id);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestRoleList(): Promise<HTTPListResponse<Role[]>> {
    try {
      const response = await this.fetcher.requestRoleList();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestCreateUser(payload: CreateUserPayload): Promise<HTTPResponse> {
    try {
      const response = await this.fetcher.requestCreateUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestEditUser(payload: EditUserPayload): Promise<HTTPResponse> {
    try {
      const response = await this.fetcher.requestEditUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestEditStatusUser(payload: EditUserStatusPayload): Promise<HTTPResponse> {
    try {
      const response = await this.fetcher.requestEditStatusUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestReinviteUser(payload: ReinviteUserPayload): Promise<HTTPResponse> {
    try {
      const response = await this.fetcher.requestReinviteUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestDeactivateUser(payload: DeactivateUserPayload): Promise<HTTPResponse> {
    try {
      const response = await this.fetcher.requestDeactivateUser(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }
}
