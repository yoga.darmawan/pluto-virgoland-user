// @ts-expect-error expected to error
import { getToken } from '@virgoland/layout';

import { Fetcher, FetcherHelper } from '@/core/fetcher';
import {
  CreateUserPayload,
  DeactivateUserPayload,
  EditUserPayload,
  EditUserStatusPayload,
  ReinviteUserPayload,
  Role,
  User,
  UserListQuery,
} from '@/domains/management/entity';
import { HTTPListResponse, HTTPResponse } from '@/domains/wrapper/entity';

const { createAxios, clientErrorHandler, clientSuccessHandler } = FetcherHelper;

export default class ManagementFetcher extends Fetcher {
  constructor() {
    super();
    this.setFetcher(createAxios('').instance);
    this.setSuccessHandler(clientSuccessHandler);
    this.setErrorHandler(clientErrorHandler);
  }

  async requestUserList(query?: UserListQuery) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('/r/user');
      this.setQueryParam(query);
      const response: HTTPListResponse<User[]> = await this.get();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestUserById(id: string) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl(`/r/user/detail/${id}`);
      this.setQueryParam(null);
      const response: HTTPResponse<User> = await this.get();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestRoleList() {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('/r/user/roles');
      this.setQueryParam(null);
      const response: HTTPListResponse<Role[]> = await this.get();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestCreateUser(payload: CreateUserPayload) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('/r/user/add');
      this.setData(payload);
      const response: HTTPResponse = await this.post();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestEditUser(payload: EditUserPayload) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('/r/user/edit');
      this.setData(payload);
      const response: HTTPResponse = await this.put();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestEditStatusUser(payload: EditUserStatusPayload) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('r/user/status');
      this.setQueryParam(null);
      this.setData(payload);
      const response: HTTPResponse = await this.put();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestReinviteUser(payload: ReinviteUserPayload) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('r/user/reinvite');
      this.setQueryParam(null);
      this.setData(payload);
      const response: HTTPResponse = await this.post();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestDeactivateUser(payload: DeactivateUserPayload) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('r/user/deactivate');
      this.setQueryParam(null);
      this.setData(payload);
      const response: HTTPResponse = await this.put();
      return response;
    } catch (error) {
      throw error;
    }
  }
}
