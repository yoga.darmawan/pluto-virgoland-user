const path = require('path');
const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react-ts');
const Dotenv = require('dotenv-webpack');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'virgoland',
    projectName: 'user-management',
    webpackConfigEnv,
    argv,
  });

  return merge(
    { ...defaultConfig, externals: ['@virgoland/layout'] },
    {
      devServer: {
        compress: true,
        port: 3001,
      },
      plugins: [
        new Dotenv({
          systemvars: true,
        }),
      ],
      resolve: {
        extensions: ['.ts', '.js', '.tsx', '.jsx'],
        alias: {
          '@/modules': path.resolve(__dirname, 'src/modules/'),
          '@/domains': path.resolve(__dirname, 'src/domains/'),
          '@/shared': path.resolve(__dirname, 'src/shared/'),
          '@/core': path.resolve(__dirname, 'src/shared/core'),
          '@/theme': path.resolve(__dirname, 'src/assets/theme'),
          '@/logos': path.resolve(__dirname, 'src/assets/images/logos'),
        },
      },
      // modify the webpack config however you'd like to by adding to this object
    },
  );
};
